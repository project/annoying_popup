<?php

namespace Drupal\annoying_popup\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * The AnnoyingPopupRequestSubscriber class.
 */
class AnnoyingPopupRequestSubscriber implements EventSubscriberInterface {

  /**
   * Re-sets cookies server side with a longer lifetime.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The GetResponseEvent.
   */
  public function extendCookieLifetime(RequestEvent $event): void {
    foreach ($event->getRequest()->cookies as $cookieName => $cookieValue) {
      if (strpos($cookieName, 'annoying_popup-') === 0 && preg_match('/(.+)-client$/', $cookieValue, $cookieActualValue) && isset($cookieActualValue[1])) {
        $event->getRequest()->cookies->remove($cookieName);
        setcookie($cookieName, $cookieActualValue[1], time() + (60 * 60 * 24 * 365), '/', '', TRUE, FALSE);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['extendCookieLifetime'];
    return $events;
  }

}
