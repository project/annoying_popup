<?php

namespace Drupal\annoying_popup;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an AnnoyingPopup entity.
 */
interface AnnoyingPopupInterface extends ConfigEntityInterface {

  /**
   * Get the AnnoyingPopup text content.
   *
   * @return array
   *   The entity's content.
   */
  public function getContent();

  /**
   * Set the AnnoyingPopup text content.
   *
   * @param array $content
   *   The AnnoyingPopup text content.
   *
   * @return self
   *   The entity.
   */
  public function setContent(array $content);

  /**
   * Get the AnnoyingPopup enabled status.
   *
   * @return bool
   *   Is the entity enabled?
   */
  public function getEnabled();

  /**
   * Set the AnnoyingPopup enabled status.
   *
   * @param bool $enabled
   *   The AnnoyingPopup enabled status.
   *
   * @return self
   *   The entity.
   */
  public function setEnabled(bool $enabled);

}
