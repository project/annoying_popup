# Annoying Popup

Got something very important to tell to your site's visitors? 
Then why not throw a popup in their face?

This module enables you to define as many HTML popups as you want. 
You can customize:

* the popup message body
* the label for the dismiss button
* an action link, in case you want to link to a page with more details, 
  or a survey or what have you
* the paths the popup should or should not appear on

Once you have dismissed or engaged with a popup, a cookie is set so the 
popup does not appear again for as long as that cookie is alive and well.

**Also**, if you use [drupal/eu_cookie_compliance](https://www.drupal.org/project/eu_cookie_compliance), popups are only initalized once the user has agreed to the use of cookies!

Popups are config entities, so you can easily export and import them.

The HTML and CSS for the popups are pretty minimal, so customizing 
their look should be easy enough.
